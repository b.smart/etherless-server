/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {EtherlessSmartReadWriteInterface, TokenInterface, /* EthereumGas, EthereumAmount */} from "@b-smart/etherless-types";
import {SoftwarePriceChanger} from "./SoftwarePriceChanger";

import {EtherlessConfigurationAWSInterface} from "./EtherlessConfigurationAWSInterface";

export class AWSSoftwarePriceChanger implements SoftwarePriceChanger {

    readonly blockchainInterface: EtherlessSmartReadWriteInterface;

    public readonly configuration: EtherlessConfigurationAWSInterface;
 
    constructor(blockchainInterface: EtherlessSmartReadWriteInterface, configuration: EtherlessConfigurationAWSInterface) {
        this.blockchainInterface = blockchainInterface;
        this.configuration = configuration;
    }

    public async onDeploy(softwareName: string, token: TokenInterface): Promise<void> {
        console.log(softwareName);
        console.log(token);
    }

    public async onUpdate(softwareName: string, token: TokenInterface): Promise<void> {
        console.log(softwareName);
        console.log(token);
    }

    public async onRemove(softwareName: string, token: TokenInterface): Promise<void> {
        console.log(softwareName);
        console.log(token);
    }

    public async onExecution(softwareName: string, token: TokenInterface, success: boolean): Promise<void> {
        console.log(softwareName);
        console.log(token);
        console.log(success);
    }
}
