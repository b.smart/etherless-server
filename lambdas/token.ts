/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {APIGatewayProxyHandler} from "aws-lambda";
import {EtherlessServerConfiguration} from "../EtherlessServerConfiguration";
import {AWSDynamoDBTokenManager} from "../AWSDynamoDBTokenManager";
import {HandlerFacade} from "../HandlerFacade";
import {EtherlessSmartReadOnly, DerivedKey} from "@b-smart/etherless-smart";
import {AWSSoftwareManager} from "../AWSSoftwareManager";
import {EtherlessException, DerivedKeyInterface} from "@b-smart/etherless-types";
import {AWSSoftwareExecutionResultLoadStore} from "../AWSSoftwareExecutionResultLoadStore";
import {AWSSoftwareMonitoring} from "../AWSSoftwareMonitoring";
import {AWSKeyLoadStore} from "../AWSKeyLoadStore";
import {WrongDerivedKeyExceptionException} from "../exceptions/WrongDerivedKeyExceptionException";
import {encrypt, decrypt} from "./crypto/crypto";

export const index: APIGatewayProxyHandler = async (event, _context) => {
    const address: string =
    event.headers.address === null || event.headers.address === undefined
        ? ""
        : (event.headers.address as string).trim();
    
    const key: string =
    event.headers.key === null || event.headers.key === undefined
        ? ""
        : (event.headers.key as string).trim();
    
    const configuration = EtherlessServerConfiguration.loadDefault(_context);
    const handlerFacadeInstance = new HandlerFacade(
        new AWSDynamoDBTokenManager(configuration),
        new EtherlessSmartReadOnly(configuration),
        new AWSSoftwareManager(configuration),
        new AWSSoftwareExecutionResultLoadStore(configuration),
        new AWSKeyLoadStore(configuration),
        new AWSSoftwareMonitoring(configuration),
        configuration
    );

    try {
        const derivedKey = new DerivedKey(key);
        const keyLoadStore = new AWSKeyLoadStore(configuration);
        const loadedKey = await keyLoadStore.load(address);
        if (loadedKey.toString() !== derivedKey.toString()) {
            throw new WrongDerivedKeyExceptionException(address, derivedKey);
        }

        const token = await handlerFacadeInstance.requestToken(address, key);

        const stringifiedToken = `${token.toString()}`;

        return {
            statusCode: 200,
            body: JSON.stringify(
                {
                    result: true,
                    token: await encrypt(stringifiedToken, derivedKey),
                    input: event
                },
                null,
                2
            )
        };
    } catch (ex) {
        console.error(ex);

        const errorMessage = (ex instanceof EtherlessException) ? ex.message : "Unknown error";

        return {
            statusCode: 200,
            body: JSON.stringify(
                {
                    result: false,
                    error: errorMessage,
                    input: event
                },
                null,
                2
            )
        };
    }
}; // token.index
