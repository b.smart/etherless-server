import * as crypto from "crypto";

import {DerivedKeyInterface} from "@b-smart/etherless-types";

export async function encrypt(plain: string, derivedKey: DerivedKeyInterface): Promise<string> {
    const iv = crypto.randomBytes(16);
    const cipher = crypto.createCipheriv("aes-256-cbc", Buffer.from(derivedKey.toString().substring(2), "hex"), iv);
    let encrypted = cipher.update(plain);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
    return JSON.stringify({iv: iv.toString("hex"), encryptedData: encrypted.toString("hex")});
}

export async function decrypt(enc: string, derivedKey: DerivedKeyInterface): Promise<string> {
    const text = JSON.parse(enc);
    const iv = Buffer.from(text.iv, "hex");
    const encryptedText = Buffer.from(text.encryptedData, "hex");
    const decipher = crypto.createDecipheriv("aes-256-cbc", Buffer.from(derivedKey.toString().substring(2), "hex"), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
}