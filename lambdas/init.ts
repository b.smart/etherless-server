/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {APIGatewayProxyHandler} from "aws-lambda";
import {EtherlessServerConfiguration} from "../EtherlessServerConfiguration";
import {AWSDynamoDBTokenManager} from "../AWSDynamoDBTokenManager";
import {HandlerFacade} from "../HandlerFacade";
import {EtherlessSmartReadOnly, DerivedKey} from "@b-smart/etherless-smart";
import {AWSSoftwareManager} from "../AWSSoftwareManager";
import {EtherlessException} from "@b-smart/etherless-types";
import {AWSSoftwareExecutionResultLoadStore} from "../AWSSoftwareExecutionResultLoadStore";
import {AWSSoftwareMonitoring} from "../AWSSoftwareMonitoring";
import {AWSKeyLoadStore} from "../AWSKeyLoadStore";

export const index: APIGatewayProxyHandler = async (event, _context) => {
    const configuration = EtherlessServerConfiguration.loadDefault(_context);
    const handlerFacadeInstance = new HandlerFacade(
        new AWSDynamoDBTokenManager(configuration),
        new EtherlessSmartReadOnly(configuration),
        new AWSSoftwareManager(configuration),
        new AWSSoftwareExecutionResultLoadStore(configuration),
        new AWSKeyLoadStore(configuration),
        new AWSSoftwareMonitoring(configuration),
        configuration
    );

    const address: string =
    event.headers.address === null || event.headers.address === undefined
        ? ""
        : (event.headers.address as string).trim();
    
    const key: string =
    event.headers.key === null || event.headers.key === undefined
        ? ""
        : (event.headers.key as string).trim();

    try {
        const clientConfiguration = await handlerFacadeInstance.init(configuration, address, new DerivedKey(key));

        return {
            statusCode: 200,
            body: JSON.stringify(
                {
                    result: true,
                    infuraProjectID: clientConfiguration.infuraProjectID,
                    infuraNetwork: clientConfiguration.infuraNetwork,
                    rpcProviderAddress: clientConfiguration.rpcProviderAddress,
                    smartAddress: clientConfiguration.smartAddress,
                    resetToBlockNumber: clientConfiguration.resetToBlockNumber,
                    input: event
                },
                null,
                2
            )
        };
    } catch (ex) {
        console.error(ex);

        const errorMessage = (ex instanceof EtherlessException) ? ex.message : "Unknown error";

        return {
            statusCode: 200,
            body: JSON.stringify(
                {
                    result: false,
                    error: errorMessage,
                    input: event
                },
                null,
                2
            )
        };
    }
}; // init.index
