import {AWSSoftwarePriceChanger} from "../../AWSSoftwarePriceChanger";
import {EtherlessServerConfiguration} from "../../EtherlessServerConfiguration";
import {EtherlessSmartReadWrite, EthereumAccount, Token} from "@b-smart/etherless-smart";
import {Handler, Context, Callback} from "aws-lambda";

export const handler: Handler = async function(event: any, context: Context) {
    event.Records.forEach((record: any) => {
        const {body} = record;

        const messageContent = JSON.parse(body);

        console.log(messageContent);
        console.log(context);

        const configuration = EtherlessServerConfiguration.loadDefault(context);

        const priceChanger = new AWSSoftwarePriceChanger(
            new EtherlessSmartReadWrite(configuration, new EthereumAccount(configuration.privateKey)),
            configuration
        );
        priceChanger.onExecution(messageContent.softwareName, new Token(messageContent.token), messageContent.success);

    });


    return {};
};