/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as AWS from "aws-sdk";

import {EtherlessConfigurationAWSInterface} from "./EtherlessConfigurationAWSInterface";
import {TokenInterface} from "@b-smart/etherless-types";
import {TokenManagerInterface} from "./TokenManagerInterface";
import {TokenOperationException} from "./exceptions/TokenOperationException";

export class AWSS3TokenManager implements TokenManagerInterface {
	public readonly serverConfiguration: EtherlessConfigurationAWSInterface;

	constructor(serverConfiguration: EtherlessConfigurationAWSInterface) {
	    this.serverConfiguration = serverConfiguration;
	}

	withServerConfiguration(serverConfiguration: EtherlessConfigurationAWSInterface): AWSS3TokenManager {
	    return new AWSS3TokenManager(serverConfiguration);
	}

	async storeToken(token: TokenInterface): Promise<void> {
	    console.log("Writing token to S3...");
		
	    const S3 = new AWS.S3();

	    const s3Params = {
	        Bucket: this.serverConfiguration.tokenBucketName,
	        Key: token.toString(),
	        Body: ""
	    } as AWS.S3.PutObjectRequest;

	    try {
	        const data: AWS.S3.PutObjectOutput = await S3.putObject(s3Params).promise();

	        console.log(data);
	    } catch (ex) {
	        const error: AWS.AWSError = ex as AWS.AWSError;

	        console.error(error);

	        throw new TokenOperationException(token);
	    }
	}

	async removeToken(token: TokenInterface): Promise<void> {
	    console.log(`Deleting token ${token}...`);

	    const S3 = new AWS.S3();

	    const s3Params = {
	        Bucket: this.serverConfiguration.tokenBucketName,
	        Key: token.toString()
	    } as AWS.S3.DeleteObjectRequest;

	    try {
	        const data: AWS.S3.DeleteObjectOutput = await S3.deleteObject(s3Params).promise();

	        console.log(data);
	    } catch (ex) {
	        const error: AWS.AWSError = ex as AWS.AWSError;

	        console.error(error);

	        throw new TokenOperationException(token);
	    }
	}

	async checkToken(token: TokenInterface): Promise<boolean> {
	    console.log(`Checking for token ${token}...`);

	    const S3 = new AWS.S3();

	    const s3Params = {
	        Bucket: this.serverConfiguration.tokenBucketName,
	        Prefix: token.toString()
	    };

	    try {
	        const data: AWS.S3.ListObjectsV2Output = await S3.listObjectsV2(s3Params).promise();

	        console.log(data);

	        if (typeof data.Contents === "undefined") {
	            return false;
	        }

	        return data.Contents.filter((s3Obj) => {
	            if (s3Obj.Key === token.toString()) {
	                return true;
	            }
	        }).length > 0;
	    } catch (ex) {
	        const error: AWS.AWSError = ex as AWS.AWSError;

	        console.error(error);

	        throw new TokenOperationException(token);
	    }
	}
}
