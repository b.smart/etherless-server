import {PackagedSoftware, SoftwareExecutionResult, SoftwareExecutionRequest} from "@b-smart/etherless-types";
import {SoftwareManagerInterface} from "../SoftwareManagerInterface";
import {AWSComponent} from "../exceptions/SoftwareOperationException";
import {SoftwareExecutionException} from "../exceptions/SoftwareExecutionException";
import JSZip from "jszip";

export class MockSoftwareManager implements SoftwareManagerInterface {

    private publishedSoftware: Map<string, string> = new Map();

    async deploy(software: PackagedSoftware): Promise<void> {
        const zip = JSZip();

        const unzipped = await zip.loadAsync(software.getZipBuffer());

        const zipData = await unzipped.files["index.js"].async("text");

        this.publishedSoftware.set(software.name, zipData);
    }

    async update(software: PackagedSoftware): Promise<void> {
        const zip = JSZip();
        const unzipped = await zip.loadAsync(software.getZipBuffer());

        const zipData = await unzipped.files["index.js"].async("text");

        this.publishedSoftware.set(software.name, zipData);
    }

    async delete(softwareName: string): Promise<void> {
        this.publishedSoftware.delete(softwareName);
    }

    async run(request: SoftwareExecutionRequest): Promise<SoftwareExecutionResult> {
        const jsData = this.publishedSoftware.get(request.name);

        if (jsData === undefined) {
            throw new SoftwareExecutionException(request.name, AWSComponent.Lambda);
        }

        const result = eval(`${jsData};\n handler(${JSON.stringify(request.parameters)});`);

        const execResult = new SoftwareExecutionResult(result.statusCode, result.body);

        return execResult;
    }


}
