/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {KeyLoadStoreInterface} from "../KeyLoadStoreInterface";
import {DerivedKey} from "@b-smart/etherless-smart";


import {LoadKeyOperationException} from "../exceptions/LoadKeyOperationException";

export class MockKeyLoadStore implements KeyLoadStoreInterface {

    private keysCollection: Map<string, DerivedKey> = new Map();

    public async store(address: string, key: DerivedKey): Promise<void> {
        this.keysCollection.set(address, key);
    }

    public async load(address: string): Promise<DerivedKey> {
        const loadResult = this.keysCollection.get(address);

        if (loadResult === undefined) {
            throw new LoadKeyOperationException(address.toString());
        }

        return loadResult;
    }

    public async isStored(address: string): Promise<boolean> {
        return this.keysCollection.get(address) !== undefined;
    }

}