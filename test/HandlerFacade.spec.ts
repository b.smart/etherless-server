/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import "mocha";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
chai.use(chaiAsPromised);
const expect = chai.expect;

import {MockConfiguration} from "./MockConfiguration";
import {MockEtherlessReadWrite} from "./MockEtherlessReadWrite";
import {MockKeyLoadStore} from "./MockKeyLoadStore";
import {MockSoftwareManager} from "./MockSoftwareManager";
import {MockTokenManager} from "./MockTokenManager";
import {MockSoftwareExecutionResultLoadStore} from "./MockSoftwareExecutionResultLoadStore";
import {MockSoftwareMonitoring} from "./MockSoftwareMonitoring";
import {HandlerFacade} from "../HandlerFacade";
import {PackagedSoftware, SoftwareExecutionRequest, SoftwareDetails} from "@b-smart/etherless-types";
import {EthereumAccount, Token} from "@b-smart/etherless-smart";
import {EtherlessException,
        EthereumGas} from "@b-smart/etherless-types";
import fs from "fs";
import {SoftwarePackage} from "./SoftwarePackage";
import {TokenInterface} from "@b-smart/etherless-types";
//import {stringify} from "querystring";

/*
emulare cli, fare la chiamata a server
chiamare i metodi del facade, stessi test della cli circa
scrittura sula blockchain emulata 
attesa del risultato
*/
async function read(file: string): Promise<string> {
    return fs.readFileSync(file, {
        encoding: "utf8",
        flag: "r"
    });
}

describe("Token", () => {
    const clientAccount: EthereumAccount = new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df");

    const blockchain = new MockEtherlessReadWrite(clientAccount);

    const facade = new HandlerFacade(
        new MockTokenManager(),
        blockchain,
        new MockSoftwareManager(),
        new MockSoftwareExecutionResultLoadStore(),
        new MockKeyLoadStore(),
        new MockSoftwareMonitoring(),
        new MockConfiguration()
    );

    it("correctly fetch and validate the token", async () => {
        const provaToken: MockTokenManager = new MockTokenManager();
        const tok = Token.generateToken();
        if (await provaToken.checkToken(tok))
            facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);

            //non so il token quindi non posso fare un confronto, basta vedere che l'ha fetchato

    });
});
describe("Deploy", () => {

    const clientAccount: EthereumAccount = new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df");

    const blockchain = new MockEtherlessReadWrite(clientAccount);

    const facade = new HandlerFacade(
        new MockTokenManager(),
        blockchain,
        new MockSoftwareManager(),
        new MockSoftwareExecutionResultLoadStore(),
        new MockKeyLoadStore(),
        new MockSoftwareMonitoring(),
        new MockConfiguration()
    );

    const swPath="./test/example/sum.js";
    let software: PackagedSoftware;
    before(async function() {
        const swSource = await read(swPath);
        const pkgSoftware: SoftwarePackage = new SoftwarePackage(swSource,"softwareDiProva");
        software =  await pkgSoftware.generatePackage();
    });
    

    it("correctly write deploy event and not throw exceptions", async () => {
        

        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);
        //Valido per il test precedente 

        await blockchain.deploySoftware(software.name,reqToken, new EthereumGas(30000,"9.0","gwei"));
        await facade.deploy(software, reqToken, await clientAccount.getAddress(), clientAccount.privateKey);
        const swDetails: SoftwareDetails = await blockchain.getSoftwareDetails(software.name);
        // console.log(swDetails);
        expect(swDetails).to.be.instanceOf(SoftwareDetails);
        

    });

});

describe("List", () => {
    const clientAccount: EthereumAccount = new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df");

    const blockchain = new MockEtherlessReadWrite(clientAccount);

    const facade = new HandlerFacade(
        new MockTokenManager(),
        blockchain,
        new MockSoftwareManager(),
        new MockSoftwareExecutionResultLoadStore(),
        new MockKeyLoadStore(),
        new MockSoftwareMonitoring(),
        new MockConfiguration()
    );

    const args: Array<string> = ["1", "2", "3"];

    const swPath="./test/example/sum.js";
    let software: PackagedSoftware;
    before(async function() {
        const swSource = await read(swPath);
        const pkgSoftware: SoftwarePackage = new SoftwarePackage(swSource,"softwareDiProva");
        software =  await pkgSoftware.generatePackage();
    });
    
    

    it("Don't list any software", async () => {
        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);

        const swList = await blockchain.getSoftwareList();

        expect(swList.length).equal(0);


    });

    it("Deploy a new software", async () => {
        

        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);
        //Valido per il test precedente 

        await blockchain.deploySoftware(software.name,reqToken, new EthereumGas(30000,"9.0","gwei"));
        await facade.deploy(software, reqToken, await clientAccount.getAddress(), clientAccount.privateKey);
        const swDetails: SoftwareDetails = await blockchain.getSoftwareDetails(software.name);
        // console.log(swDetails);
        expect(swDetails).to.be.instanceOf(SoftwareDetails);
        

    });

    it("Find the new depoloyed software", async () => {

        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);

        const swList = await blockchain.getSoftwareList();

        expect(swList.length).equal(1);


    });

});

describe("Run", () => {

    const clientAccount: EthereumAccount = new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df");

    const blockchain = new MockEtherlessReadWrite(clientAccount);

    const facade = new HandlerFacade(
        new MockTokenManager(),
        blockchain,
        new MockSoftwareManager(),
        new MockSoftwareExecutionResultLoadStore(),
        new MockKeyLoadStore(),
        new MockSoftwareMonitoring(),
        new MockConfiguration()
    );

    const args: Array<string> = ["1", "2", "3"];

    const swPath="./test/example/sum.js";
    let software: PackagedSoftware;
    before(async function() {
        const swSource = await read(swPath);
        const pkgSoftware: SoftwarePackage = new SoftwarePackage(swSource,"softwareDiProva");
        software =  await pkgSoftware.generatePackage();
    });
    
    
    it("Should throw an exception: Software must exist to be executed  ", async () => {
        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);
        
          await expect( blockchain.invokeSoftware(software.name,reqToken, new EthereumGas(30000,"9.0","gwei"))).to.be.fulfilled;
    });        

    it("Deploy a new software", async () => {
        

        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);
        //Valido per il test precedente 
        await blockchain.deploySoftware(software.name,reqToken, new EthereumGas(30000,"9.0","gwei"));
        await facade.deploy(software, reqToken, await clientAccount.getAddress(), clientAccount.privateKey);
        const swDetails: SoftwareDetails = await blockchain.getSoftwareDetails(software.name);
        // console.log(swDetails);
        expect(swDetails).to.be.instanceOf(SoftwareDetails);
        

    });

    it("correctly runs the specified software", async () => {
        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);
        
        //Write the event on blockchain
        await blockchain.invokeSoftware(software.name,reqToken, new EthereumGas(30000,"9.0","gwei"));
        //Scrittura evento sulla blockchain 
        const swExec: SoftwareExecutionRequest = new SoftwareExecutionRequest(software.name, args);
        await facade.run(swExec, reqToken, await clientAccount.getAddress(), clientAccount.privateKey);
        const swDetails: SoftwareDetails = await blockchain.getSoftwareDetails(software.name);
        expect(swDetails).to.be.instanceOf(SoftwareDetails);
        //expect(await facade.run(request, Token.generateToken(), await blockchain.getAddress(), clientAccount.privateKey)).to.equal("tfdIk");
    });


});

describe("Update", () =>{

    const clientAccount: EthereumAccount = new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df");

    const blockchain = new MockEtherlessReadWrite(clientAccount);

    const facade = new HandlerFacade(
        new MockTokenManager(),
        blockchain,
        new MockSoftwareManager(),
        new MockSoftwareExecutionResultLoadStore(),
        new MockKeyLoadStore(),
        new MockSoftwareMonitoring(),
        new MockConfiguration()
    );

    const args: Array<string> = ["1", "2", "3"];

    const swPath="./test/example/sum.js";
    let software: PackagedSoftware;
    before(async function() {
        const swSource = await read(swPath);
        const pkgSoftware: SoftwarePackage = new SoftwarePackage(swSource,"softwareDiProva");
        software =  await pkgSoftware.generatePackage();
    });
    
    it("Should throw an exception: Software must exist to be updated  ", async () => {
        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);
        
          await expect( blockchain.updateSoftware(software.name,reqToken, new EthereumGas(30000,"9.0","gwei"))).to.be.rejected;
    });    

    it("correctly deploy a software", async () => {
        

        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);

        await blockchain.deploySoftware(software.name,reqToken, new EthereumGas(30000,"9.0","gwei"));

        const swDetails: SoftwareDetails = await blockchain.getSoftwareDetails(software.name);
        // console.log(swDetails);
        expect(swDetails).to.be.instanceOf(SoftwareDetails);
        

    });

    it("correctly updates the previous deployed software", async () => {
        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);
        //Valido per il test precedente 
        await blockchain.updateSoftware(software.name,reqToken, new EthereumGas(30000,"9.0","gwei"));
        await facade.update(software, reqToken,  await clientAccount.getAddress(), clientAccount.privateKey);
        //if not throw errors is ok 
        const swDetails: SoftwareDetails = await blockchain.getSoftwareDetails(software.name);
        expect(swDetails).to.be.instanceOf(SoftwareDetails);
        
    });


});



describe("Remove", () => {

    const clientAccount: EthereumAccount = new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df");

    const blockchain = new MockEtherlessReadWrite(clientAccount);

    const facade = new HandlerFacade(
        new MockTokenManager(),
        blockchain,
        new MockSoftwareManager(),
        new MockSoftwareExecutionResultLoadStore(),
        new MockKeyLoadStore(),
        new MockSoftwareMonitoring(),
        new MockConfiguration()
    );

    const swPath="./test/example/sum.js";
    let software: PackagedSoftware;
    before(async function() {
        const swSource = await read(swPath);
        const pkgSoftware: SoftwarePackage = new SoftwarePackage(swSource,"softwareDiProva");
        software =  await pkgSoftware.generatePackage();
    });
    

    it("Should throw an exception: Software must exist to be removed  ", async () => {
        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);
        
          await expect( blockchain.removeSoftware(software.name,reqToken, new EthereumGas(30000,"9.0","gwei"))).to.be.rejected;
    });    
        
    it("Deploy a new software", async () => {    

        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);
        //Valido per il test precedente 

        await blockchain.deploySoftware(software.name,reqToken, new EthereumGas(30000,"9.0","gwei"));
        await facade.deploy(software, reqToken, await clientAccount.getAddress(), clientAccount.privateKey);
        const swDetails: SoftwareDetails = await blockchain.getSoftwareDetails(software.name);
        // console.log(swDetails);
        expect(swDetails).to.be.instanceOf(SoftwareDetails);
    });
    
    it("Correctly deletes the previous deployed software", async () => {
        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);

        await blockchain.removeSoftware(software.name,reqToken, new EthereumGas(30000,"9.0","gwei"));

        //expect ?? bisogna solo dirgli che non dovrebbe throware errori

    });



});

describe("log", () => {

    const clientAccount: EthereumAccount = new EthereumAccount("0xd6d22c1cee9fbc164d638de6d0ec1abe23dd200cfb66c9ae648d6cdff3a170df");

    const blockchain = new MockEtherlessReadWrite(clientAccount);

    const facade = new HandlerFacade(
        new MockTokenManager(),
        blockchain,
        new MockSoftwareManager(),
        new MockSoftwareExecutionResultLoadStore(),
        new MockKeyLoadStore(),
        new MockSoftwareMonitoring(),
        new MockConfiguration()
    );

    const args: Array<string> = ["1", "2", "3"];

    const swPath="./test/example/sum.js";
    let software: PackagedSoftware;
    
    let execToken: TokenInterface;
    before(async function() {
        const swSource = await read(swPath);
        const pkgSoftware: SoftwarePackage = new SoftwarePackage(swSource,"softwareDiProva");
        software =  await pkgSoftware.generatePackage();
        execToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);
    });

    it("Should throw an error: can not log a not existing execution", async () => {
        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);
        //Write the event on blockchain
        await expect (facade.log(reqToken,await clientAccount.getAddress(), clientAccount.privateKey)).to.be.rejected;
    })

    it("Deploy a new software", async () => {
        

        const reqToken = await facade.requestToken(await blockchain.getAddress(), clientAccount.privateKey);
        //Valido per il test precedente 
        await blockchain.deploySoftware(software.name,reqToken, new EthereumGas(30000,"9.0","gwei"));
        await facade.deploy(software, reqToken, await clientAccount.getAddress(), clientAccount.privateKey);
        const swDetails: SoftwareDetails = await blockchain.getSoftwareDetails(software.name);
        // console.log(swDetails);
        expect(swDetails).to.be.instanceOf(SoftwareDetails);
        

    });

    it("correctly runs the specified software", async () => {
        
        //Write the event on blockchain
        await blockchain.invokeSoftware(software.name,execToken, new EthereumGas(30000,"9.0","gwei"));
        //Scrittura evento sulla blockchain 
        const swExec: SoftwareExecutionRequest = new SoftwareExecutionRequest(software.name, args);
        await facade.run(swExec, execToken, await clientAccount.getAddress(), clientAccount.privateKey);
        const swDetails: SoftwareDetails = await blockchain.getSoftwareDetails(software.name);
        expect(swDetails).to.be.instanceOf(SoftwareDetails);
        //expect(await facade.run(request, Token.generateToken(), await blockchain.getAddress(), clientAccount.privateKey)).to.equal("tfdIk");
    });

    it("correctly log the last software execution", async () => {
        await facade.log(execToken,await clientAccount.getAddress(), clientAccount.privateKey)
    })


});
