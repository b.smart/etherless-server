/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {
    EtherlessException,
    SoftwareDetails,
    EtherlessSmartReadWriteInterface,
    EtherlessSoftwareUpgradeOperation,
    EthereumGas,
    EthereumAmount,
    EtherlessSoftwareOperationInterface,
    EtherlessSoftwareDeployOperation,
    EtherlessSoftwareRemovalOperation,
    EtherlessSoftwareExecutionOperation,
    TokenInterface,
    EthereumAccountInterface,
    EthereumConnectionConfigurationInterface
} from "@b-smart/etherless-types";
import { EthereumConnectionConfiguration } from "@b-smart/etherless-smart";

export class MockEtherlessReadWrite implements EtherlessSmartReadWriteInterface {

    public readonly account: EthereumAccountInterface

    public readonly connectionConfiguration: EthereumConnectionConfigurationInterface

    private software: Map<string, SoftwareDetails> = new Map();

    private events: Array<EtherlessSoftwareOperationInterface> = [];

    private wallet: Map<string, number> = new Map();

    constructor(account: EthereumAccountInterface) {
        this.account = account;
        this.connectionConfiguration = new EthereumConnectionConfiguration();
        this.software = new Map();
        this.wallet = new Map();
    }

    public async getBalance(address: string): Promise<EthereumAmount> {
        return new EthereumAmount("50", "gwei");
    }

    async getBlockNumber(): Promise<number> {
        return 0;
    }

    async getAddress(): Promise<string> {
        return "0x978y378fd12bniufnbe8i9u21hfiu2noijlklkvdmfikjngkl";
    }

    async getSoftwareDetails(name: string): Promise<SoftwareDetails> {
        //     if(this.software.has(name))//name===
        //         return new SoftwareDetails(name, new Date, new Date, );
        /* return new SoftwareDetails(
            name,
            new Date(1000),
            new Date(1000),
            5,
            new EthereumAmount('50000', "wei"),
            'b-smart'
        ); */

        const details = this.software.get(name);

        if (typeof details === "undefined") {
            throw ("aaahhh");
        }

        return details;
    }

    async initialize(gas: EthereumGas | undefined): Promise<void> {
        // nothing to be done.
    }


    async softwareExists(name: string): Promise<boolean> {
        return this.software.has(name);
    }

    async deploySoftware(name: string, token: TokenInterface, gas: EthereumGas | undefined): Promise<void> {
        this.events.push(new EtherlessSoftwareDeployOperation(await this.account.getAddress(), name, token));
        this.software.set(name, new SoftwareDetails(
            name,
            new Date(),
            new Date(),
            0,
            new EthereumAmount("1000", "gwei"),
            "b-smart" // To change
        ));
    }

    async updateSoftware(name: string, token: TokenInterface, gas: EthereumGas | undefined): Promise<void> {
        this.events.push(new EtherlessSoftwareUpgradeOperation(await this.account.getAddress(), name, token));

        const oldSw: SoftwareDetails | undefined = await this.software.get(name); // Old sw config

        // Se non esiste -> undefined 
        if (typeof oldSw === "undefined")
            throw new EtherlessException("The software specified does not exists");
        else
            this.software.set(name, new SoftwareDetails(
                name,
                oldSw.publishDate,
                new Date(),
                (oldSw.updateCount) + 1,
                new EthereumAmount("1000", "gwei"),
                "b-smart" // Da cambiare
            ));
    }

    async removeSoftware(name: string, token: TokenInterface, gas: EthereumGas | undefined): Promise<void> {
        if (this.software.get(name) === undefined) {
            throw new EtherlessException("Software must exists to be removed");
        }

        this.software.delete(name);

        this.events.push(new EtherlessSoftwareRemovalOperation(await this.account.getAddress(), name, token));
    }

    async invokeSoftware(name: string, token: TokenInterface, gas: EthereumGas | undefined): Promise<void> {
        this.events.push(new EtherlessSoftwareExecutionOperation(await this.account.getAddress(), name, token));
    }

    async getFirstSoftwarePublicationEvent(
        from: string | undefined,
        name: string | undefined,
        token: TokenInterface | undefined,
        timeout: number | undefined
    ): Promise<EtherlessSoftwareDeployOperation> {
        for (let event of this.events) {
            if (event instanceof EtherlessSoftwareDeployOperation)
                if (!(
                    ((typeof name !== "undefined") && (event.name !== name)) ||
                    ((typeof from !== "undefined") && (event.address !== from)) ||
                    ((typeof token !== "undefined") && (event.token.toString() !== token.toString())))
                )
                    return event;
        }

        throw new EtherlessException("DeployOperation event not found");
    }

    async getFirstSoftwareUpdateEvent(
        from: string | undefined,
        name: string | undefined,
        token: TokenInterface | undefined,
        timeout: number | undefined
    ): Promise<EtherlessSoftwareUpgradeOperation> {

        for (let event of this.events) {
            if (event instanceof EtherlessSoftwareUpgradeOperation)
                if (!(
                    ((typeof name !== "undefined") && (event.name !== name)) ||
                    ((typeof from !== "undefined") && (event.address !== from)) ||
                    ((typeof token !== "undefined") && (event.token.toString() !== token.toString())))
                )
                    return event;
        }

        throw new EtherlessException("SoftwareUpgrade event not found ");
    }

    async getFirstSoftwareRemovalEvent(
        from: string | undefined,
        name: string | undefined,
        token: TokenInterface | undefined,
        timeout: number | undefined
    ): Promise<EtherlessSoftwareRemovalOperation> {

        for (let event of this.events) {
            if (event instanceof EtherlessSoftwareRemovalOperation)
                if (!(
                    ((typeof name !== "undefined") && (event.name !== name)) ||
                    ((typeof from !== "undefined") && (event.address !== from)) ||
                    ((typeof token !== "undefined") && (event.token.toString() !== token.toString())))
                )
                    return event;
        }

        throw new EtherlessException("Removal event not found ");

    }

    async getFirstSoftwareExecutionEvent(
        from: string | undefined,
        name: string | undefined,
        token: TokenInterface | undefined,
        timeout: number | undefined
    ): Promise<EtherlessSoftwareExecutionOperation> {
        for (let event of this.events) {
            // console.log(event);
            if (event instanceof EtherlessSoftwareExecutionOperation)
                if (!(
                    ((typeof name !== "undefined") && (event.name !== name)) ||
                    ((typeof from !== "undefined") && (event.address !== from)) ||
                    ((typeof token !== "undefined") && (event.token.toString() !== token.toString())))
                )
                    return event;
        }
        throw new EtherlessException("ExecutionOperation event not found ");
    }


    async getSoftwareList(): Promise<Array<Promise<SoftwareDetails>>> {
        const swList: Array<string> = [];

        const response: Array<Promise<SoftwareDetails>> = [];
        this.software.forEach((value: SoftwareDetails, key: string) => {
            response.push(
                this.getSoftwareDetails(key)
            );
        });


        return response;

    }

    async getAllSoftwareExecutionByAddress(from: string): Promise<Array<EtherlessSoftwareExecutionOperation>> {
        return this.events.filter((value: EtherlessSoftwareOperationInterface) => {
            return (value instanceof EtherlessSoftwareExecutionOperation) && (value.address === from); // era value.address
        });
    }

    async deposit(amount: EthereumAmount, gas: EthereumGas | undefined): Promise<void> {
        // empty.
    }

    async withdraw(gas: EthereumGas | undefined): Promise<void> {
        // empty.
    }

    async changeSoftwarePrice(name: string, amount: EthereumAmount, gas: EthereumGas | undefined): Promise<void> {
        // empty.
    }

}
