import JSZip from "jszip";

import {PackagedSoftware} from "@b-smart/etherless-types";

export class SoftwarePackage {

    public readonly source: string

    public readonly name: string

    public readonly version: string

    constructor(source: string, name: string, version = "0.0.1") {
        this.name = name;
        this.version = version;
        this.source = source;
    }

    public withSource(source: string): SoftwarePackage {
        return new SoftwarePackage(source, this.name, this.version);
    }

    public withName(name: string): SoftwarePackage {
        return new SoftwarePackage(this.source, name, this.version);
    }

    public withVersion(version: string): SoftwarePackage {
        return new SoftwarePackage(this.source, this.name, version);
    }

    public async generatePackage(): Promise<PackagedSoftware> {
        const zip = new JSZip();
        zip.file("index.js", this.source);
        zip.file("package.json", JSON.stringify({
            "name": this.name,
            "version": this.version,
            "main": "index.js",
            "dependencies": {}
        }));
        
        return new PackagedSoftware(this.name, await zip.generateAsync({type: "base64"}));
    }
}