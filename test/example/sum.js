function handler(event) {
    const arrayOfNumbers = [];
	event.forEach(element => {
		arrayOfNumbers.push(Number(element));
	});

	let sum = 0.0;
	arrayOfNumbers.forEach((value => {
		sum += value
	}))

	return {
		statusCode: 200,
		body: JSON.stringify(
			{
				result: "sum v1.0.0, result is: " + sum,
				input: event
			},
			null,
			2
		)
	};
};
