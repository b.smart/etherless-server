import {AWSComponent, SoftwareOperationException} from "./SoftwareOperationException";

export class SoftwareUpdateException extends SoftwareOperationException {

    constructor(name: string, erroringComponent: AWSComponent) {
        super(`Error while updating ${name}`, name, erroringComponent);
    }

}