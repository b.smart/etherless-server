/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {PackagedSoftware, SoftwareExecutionResult, SoftwareExecutionRequest} from "@b-smart/etherless-types";
import {EtherlessConfigurationAWSInterface} from "./EtherlessConfigurationAWSInterface";
import {AWSComponent} from "./exceptions/SoftwareOperationException";
import {SoftwareDeployException} from "./exceptions/SoftwareDeployException";
import {SoftwareRemoveException} from "./exceptions/SoftwareRemoveException";
import {SoftwareUpdateException} from "./exceptions/SoftwareUpdateException";
import {SoftwareExecutionException} from "./exceptions/SoftwareExecutionException";
import {SoftwareManagerInterface} from "./SoftwareManagerInterface";

import * as AWS from "aws-sdk";

export class AWSSoftwareManager implements SoftwareManagerInterface {
	public readonly serverConfiguration: EtherlessConfigurationAWSInterface;

	constructor(serverConfiguration: EtherlessConfigurationAWSInterface) {
	    this.serverConfiguration = serverConfiguration;
	}

	withServerConfiguration(serverConfiguration: EtherlessConfigurationAWSInterface): AWSSoftwareManager {
	    return new AWSSoftwareManager(serverConfiguration);
	}

	async deploy(software: PackagedSoftware): Promise<void> {
	    const S3 = new AWS.S3();
	    const lambda = new AWS.Lambda({region: this.serverConfiguration.region});

	    const s3Params = {
	        Bucket: this.serverConfiguration.softwareSourcesBucketName,
	        Key: `${software.name}.zip`,
	        Body: software.getZipBuffer()
	    } as AWS.S3.PutObjectRequest;

	    try {
	        console.log(`Writing source on S3 for software ${software.name}...`);
	        const srcUploadResult = await S3.putObject(s3Params).promise();
	        console.log(srcUploadResult);
	    } catch (ex) {
	        const error = ex as AWS.AWSError;
	        console.error(error);

	        throw new SoftwareDeployException(software.name, AWSComponent.S3);
	    }

	    try {
	        const lambdaParams = {
	            Code: {
	                S3Bucket: s3Params.Bucket,
	                S3Key: s3Params.Key
	            },
	            // Lambda function creation based o//
	            FunctionName: software.name,
	            Handler: "index.handler",
	            MemorySize: this.serverConfiguration.lambdaMemorySize,
	            Publish: true,
	            Role: this.serverConfiguration.lambdaIAMRole,
	            Runtime: this.serverConfiguration.lambdaRuntime,
	            Timeout: 3,
	            VpcConfig: {}
	        } as AWS.Lambda.CreateFunctionRequest;

	        console.log(`Creating lambda for software ${software.name}...`);
	        const result: AWS.Lambda.FunctionConfiguration = await lambda.createFunction(lambdaParams).promise();
	        console.log(result);
	    } catch (ex) {
	        const error = ex as AWS.AWSError;
	        console.error(error);

	        throw new SoftwareDeployException(software.name, AWSComponent.Lambda);
	    }

	    try {
	        const s3DeleteParams = {
	            Bucket: s3Params.Bucket,
	            Key: s3Params.Key
	        } as AWS.S3.DeleteObjectRequest;

	        console.log(`Deleting source from S3 for software ${software.name}...`);
	        const srcDeleteResult = await S3.deleteObject(s3DeleteParams).promise();
	        console.log(srcDeleteResult);
	    } catch (ex) {
	        const error = ex as AWS.AWSError;
	        console.error(error);

	        throw new SoftwareRemoveException(software.name, AWSComponent.S3);
	    }
	}

	async update(software: PackagedSoftware): Promise<void> {
	    const S3 = new AWS.S3();
	    const lambda = new AWS.Lambda({region: this.serverConfiguration.region});

	    const s3PutParams = {
	        Bucket: this.serverConfiguration.softwareSourcesBucketName,
	        Key: `${software.name}.zip`,
	        Body: software.getZipBuffer()
	    } as AWS.S3.PutObjectRequest;

	    try {
	        console.log(`Writing source on S3 for software ${software.name}...`);
	        const srcStoreResult = await S3.putObject(s3PutParams).promise();
	        console.log(srcStoreResult);
	    } catch (ex) {
	        const error = ex as AWS.AWSError;
	        console.error(error);

	        throw new SoftwareUpdateException(software.name, AWSComponent.S3);
	    }

	    try {
	        const lambdaParams = {
	            FunctionName: software.name,
	            S3Bucket: s3PutParams.Bucket,
	            S3Key: s3PutParams.Key,
	            Publish: true
	        } as AWS.Lambda.UpdateFunctionCodeRequest;

	        console.log(`Updating function code for lambda ${software.name}...`);
	        const result = await lambda.updateFunctionCode(lambdaParams).promise();
	        console.log(result);
	    } catch (ex) {
	        const error = ex as AWS.AWSError;
	        console.error(error);

	        throw new SoftwareUpdateException(software.name, AWSComponent.Lambda);
	    }

	    try {
	        const s3Params = {
	            Bucket: s3PutParams.Bucket,
	            Key: s3PutParams.Key
	        } as AWS.S3.DeleteObjectRequest;

	        console.log(`Deleting source from S3 for software ${software.name}...`);
	        const srcDeleteResult = await S3.deleteObject(s3Params).promise();
	        console.log(srcDeleteResult);
	    } catch (ex) {
	        const error = ex as AWS.AWSError;
	        console.error(error);

	        throw new SoftwareRemoveException(software.name, AWSComponent.S3);
	    }
	}

	async delete(softwareName: string): Promise<void> {
	    const lambda = new AWS.Lambda({region: this.serverConfiguration.region});

	    const lambdaParams = {
	        FunctionName: softwareName
	    } as AWS.Lambda.DeleteFunctionRequest;

	    try {
	        console.log(`Removing lambda ${softwareName}...`);
	        const result = await lambda.deleteFunction(lambdaParams).promise();
	        console.log(result);

	    } catch (ex) {
	        const error = ex as AWS.AWSError;
	        console.error(error);

	        throw new SoftwareRemoveException(softwareName, AWSComponent.Lambda);
	    }
	}

	async run(request: SoftwareExecutionRequest): Promise<SoftwareExecutionResult> {
	    const lambda = new AWS.Lambda({region: this.serverConfiguration.region});

	    let stringifiedResult: string | undefined = undefined;

	    try {
	        const lambdaParams = {
	            FunctionName: request.name,
	            Payload: JSON.stringify(request.parameters)
	        } as AWS.Lambda.InvocationRequest;

	        console.log(`Running lambda ${request.name}...`);
	        const result: AWS.Lambda.InvocationResponse = await lambda.invoke(lambdaParams).promise();
	        console.log(result);

	        if (result.Payload instanceof Buffer) {
	            stringifiedResult = result.Payload.toString();
	        } else if (result.Payload instanceof Uint8Array) {
	            stringifiedResult = result.Payload.toString();
	        } else if (typeof result.Payload === "undefined") {
	            stringifiedResult = "";
	        } else if (typeof result.Payload === "string") {
	            stringifiedResult = result.Payload;
	        } else {
	            stringifiedResult = "";

	            console.error("Blob ?!?!?");
	        }
	    } catch (ex) {
	        const error = ex as AWS.AWSError;
	        console.error(error);

	        throw new SoftwareExecutionException(request.name, AWSComponent.Lambda);
	    }

	    if (typeof stringifiedResult === "undefined") {
	        return new SoftwareExecutionResult(500, "Lambda function code not found -> Lambda is empty");
	    }

	    const executionResult = JSON.parse(stringifiedResult);

	    return new SoftwareExecutionResult(Number(executionResult.statusCode), executionResult.body.toString());
	}
}
