/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {
    TimedoutSoftwareDeployBlockchainSearch,
    TimedoutSoftwareUpdateBlockchainSearch,
    TimedoutSoftwareRemoveBlockchainSearch,
    TimedoutSoftwareExecutionBlockchainSearch
} from "./exceptions/TimedoutSoftwareOperationBlockchainSearch";
import {
    PackagedSoftware,
    EtherlessSmartReadOnlyInterface,
    SoftwareExecutionRequest,
    SoftwareExecutionResult,
    TokenInterface,
    DerivedKeyInterface,
    EtherlessServerInterface,
    EthereumConnectionConfigurationInterface
} from "@b-smart/etherless-types";
import {Token} from "@b-smart/etherless-smart";
import {TokenManagerInterface} from "./TokenManagerInterface";
import {SoftwareManagerInterface} from "./SoftwareManagerInterface";
import {EthereumConfigurationServerInterface} from "./EthereumConfigurationServerInterface";
import {UnverifiedTokenException} from "./exceptions/UnverifiedTokenException";
import {SoftwareExecutionResultLoadStoreInterface} from "./SoftwareExecutionResultLoadStoreInterface";
import {SoftwareMonitoringInterface} from "./SoftwareMonitoringInterface";
import {KeyLoadStoreInterface} from "./KeyLoadStoreInterface";

export class HandlerFacade implements EtherlessServerInterface {

	private readonly ethereumConnection: EtherlessSmartReadOnlyInterface;

	private readonly tokenManager: TokenManagerInterface;

	private readonly softwareManager: SoftwareManagerInterface;

	private readonly serverConfiguration: EthereumConfigurationServerInterface;

	private readonly softwareMonitoring: SoftwareMonitoringInterface;

	private readonly keyLoadStore: KeyLoadStoreInterface;

	private readonly softwareLoadStore: SoftwareExecutionResultLoadStoreInterface;

	constructor(
	    tokenManager: TokenManagerInterface,
	    ethereumConnection: EtherlessSmartReadOnlyInterface,
	    softwareManager: SoftwareManagerInterface,
	    softwareLoadStore: SoftwareExecutionResultLoadStoreInterface,
	    keyLoadStore: KeyLoadStoreInterface,
	    softwareMonitoring: SoftwareMonitoringInterface,
	    serverConfiguration: EthereumConfigurationServerInterface
	) {
	    this.tokenManager = tokenManager;

	    this.ethereumConnection = ethereumConnection;

	    this.softwareManager = softwareManager;

	    this.softwareLoadStore = softwareLoadStore;

	    this.softwareMonitoring = softwareMonitoring;

	    this.keyLoadStore = keyLoadStore;

	    this.serverConfiguration = serverConfiguration;
	}

	async requestToken(address: string, key: DerivedKeyInterface): Promise<TokenInterface> {
	    const token = Token.generateToken();

	    await this.tokenManager.storeToken(token);

	    return token;
	}

	async init(outdatedConfiguration: EthereumConnectionConfigurationInterface, address: string, key: DerivedKeyInterface): Promise<EthereumConnectionConfigurationInterface> {
	    if (!(await this.keyLoadStore.isStored(outdatedConfiguration.smartAddress))) {
	        await this.keyLoadStore.store(address, key);
	    }

	    return outdatedConfiguration;
	}

	async log(token: TokenInterface, address: string, derivedKey: DerivedKeyInterface): Promise<SoftwareExecutionResult> {
	    const blockchainSoftwareOperation = await this.ethereumConnection.getFirstSoftwareExecutionEvent(
	        address,
	        undefined,
	        token,
	        this.serverConfiguration.waitTimeout
	    );

	    if ((!blockchainSoftwareOperation) || (blockchainSoftwareOperation.token.toString() !== token.toString())) {
	        throw new TimedoutSoftwareExecutionBlockchainSearch("", token.toString());
	    }

	    const executionResult = await this.softwareLoadStore.load(blockchainSoftwareOperation);
	    // const key = await this.keyLoadStore.load(blockchainSoftwareOperation.address);

	    return executionResult;
	}

	async run(request: SoftwareExecutionRequest, token: TokenInterface, address: string, derivedKey: DerivedKeyInterface): Promise<SoftwareExecutionResult> {
	    if (!await this.tokenManager.checkToken(token)) {
	        throw new UnverifiedTokenException(token);
	    }

	    // remove the used token
	    await this.tokenManager.removeToken(token);

	    const blockchainSoftwareOperation = await this.ethereumConnection.getFirstSoftwareExecutionEvent(
	        address,
	        request.name,
	        token,
	        this.serverConfiguration.waitTimeout
	    );

	    if ((!blockchainSoftwareOperation) || (blockchainSoftwareOperation.token.toString() !== token.toString()) || (blockchainSoftwareOperation.name !== request.name)) {
	        throw new TimedoutSoftwareExecutionBlockchainSearch(request.name, token.toString());
	    }

	    const result = await this.softwareManager.run(request);
	    await this.softwareLoadStore.store(result, blockchainSoftwareOperation);

	    try {
	        await this.softwareMonitoring.onExecution(request.name, token, true);
	    } catch (ex) {
	        console.error(ex);
	    }

	    return result;
	}

	async deploy(software: PackagedSoftware, token: TokenInterface, address: string, derivedKey: DerivedKeyInterface): Promise<void> {
	    if (!await this.tokenManager.checkToken(token)) {
	        throw new UnverifiedTokenException(token);
	    }

	    // remove the used token
	    await this.tokenManager.removeToken(token);

	    const blockchainSoftwareOperation = await this.ethereumConnection.getFirstSoftwarePublicationEvent(
	        address,
	        software.name,
	        token,
	        this.serverConfiguration.waitTimeout
	    );

	    if ((!blockchainSoftwareOperation) || (blockchainSoftwareOperation.token.toString() !== token.toString()) || (blockchainSoftwareOperation.name !== software.name)) {
	        throw new TimedoutSoftwareDeployBlockchainSearch(software.name, token.toString());
	    }

	    await this.softwareManager.deploy(software);

	    try {
	        await this.softwareMonitoring.onDeploy(software.name, token);
	    } catch (ex) {
	        console.error(ex);
	    }
	}

	async update(software: PackagedSoftware, token: TokenInterface, address: string, derivedKey: DerivedKeyInterface): Promise<void> {
	    if (!await this.tokenManager.checkToken(token)) {
	        throw new UnverifiedTokenException(token);
	    }

	    // remove the used token
	    await this.tokenManager.removeToken(token);

	    const blockchainSoftwareOperation = await this.ethereumConnection.getFirstSoftwareUpdateEvent(
	        address,
	        software.name,
	        token,
	        this.serverConfiguration.waitTimeout
	    );

	    if ((!blockchainSoftwareOperation) || (blockchainSoftwareOperation.token.toString() !== token.toString()) || (blockchainSoftwareOperation.name !== software.name)) {
	        throw new TimedoutSoftwareUpdateBlockchainSearch(software.name, token.toString());
	    }

	    await this.softwareManager.update(software);

	    try {
	        await this.softwareMonitoring.onUpdate(software.name, token);
	    } catch (ex) {
	        console.error(ex);
	    }
	}

	async remove(softwareName: string, token: TokenInterface, address: string, derivedKey: DerivedKeyInterface): Promise<void> {
	    if (!await this.tokenManager.checkToken(token)) {
	        throw new UnverifiedTokenException(token);
	    }

	    // remove the used token
	    await this.tokenManager.removeToken(token);

	    const blockchainSoftwareOperation = await this.ethereumConnection.getFirstSoftwareRemovalEvent(
	        address,
	        softwareName,
	        token,
	        this.serverConfiguration.waitTimeout
	    );

	    if ((!blockchainSoftwareOperation) || (blockchainSoftwareOperation.token.toString() !== token.toString()) || (blockchainSoftwareOperation.name !== softwareName)) {
	        throw new TimedoutSoftwareRemoveBlockchainSearch(softwareName, token.toString());
	    }

	    await this.softwareManager.delete(softwareName);

	    try {
	        await this.softwareMonitoring.onRemove(softwareName, token);
	    } catch (ex) {
	        console.error(ex);
	    }
	}
}
