/*
 * MIT License
 *
 * Copyright (c) 2020 B.smart
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {TokenInterface} from "@b-smart/etherless-types";
import {SoftwareMonitoringInterface} from "./SoftwareMonitoringInterface";

import * as AWS from "aws-sdk";
import {EtherlessConfigurationAWSInterface} from "./EtherlessConfigurationAWSInterface";

export class AWSSoftwareMonitoring implements SoftwareMonitoringInterface {

    public readonly configuration: EtherlessConfigurationAWSInterface

    constructor(configuration: EtherlessConfigurationAWSInterface) {
        this.configuration = configuration;
    }

    public async onDeploy(softwareName: string, token: TokenInterface): Promise<void> {
        if (typeof this.configuration.deployQueueName === "undefined") {
            console.warn("no SQS deploy queue Name. Skipping monitoring.");
            return;
        }

        const sqs = new AWS.SQS();

        const params = {
            MessageBody: JSON.stringify({
                softwareName: softwareName,
                token: token.toString(),
            }),
            QueueUrl: `https://sqs.${this.configuration.region}.amazonaws.com/${this.configuration.accountId}/${this.configuration.deployQueueName}`
        } as AWS.SQS.SendMessageRequest;

        try {
            console.log(`Sending monitoring deploy signal using SQS for software ${softwareName}...`);
            const data: AWS.SQS.SendMessageResult = await sqs.sendMessage(params).promise();
            console.log(data);
        } catch (ex) {
            const error: AWS.AWSError = ex as AWS.AWSError;

            console.error(error);
        }
    }

    public async onUpdate(softwareName: string, token: TokenInterface): Promise<void> {
        if (typeof this.configuration.updateQueueName === "undefined") {
            console.warn("no SQS update queue Name. Skipping monitoring.");
            return;
        }

        const sqs = new AWS.SQS();

        const params = {
            MessageBody: JSON.stringify({
                softwareName: softwareName,
                token: token.toString(),
            }),
            QueueUrl: `https://sqs.${this.configuration.region}.amazonaws.com/${this.configuration.accountId}/${this.configuration.updateQueueName}`
        } as AWS.SQS.SendMessageRequest;

        try {
            console.log(`Sending monitoring update signal using SQS for software ${softwareName}...`);
            const data: AWS.SQS.SendMessageResult = await sqs.sendMessage(params).promise();
            console.log(data);
        } catch (ex) {
            const error: AWS.AWSError = ex as AWS.AWSError;

            console.error(error);
        }

    }

    public async onRemove(softwareName: string, token: TokenInterface): Promise<void> {
        if (typeof this.configuration.removeQueueName === "undefined") {
            console.warn("no SQS remove queue Name. Skipping monitoring.");
            return;
        }

        const sqs = new AWS.SQS();

        const params = {
            MessageBody: JSON.stringify({
                softwareName: softwareName,
                token: token.toString(),
            }),
            QueueUrl: `https://sqs.${this.configuration.region}.amazonaws.com/${this.configuration.accountId}/${this.configuration.removeQueueName}`
        } as AWS.SQS.SendMessageRequest;

        try {
            console.log(`Sending monitoring delete signal using SQS for software ${softwareName}...`);
            const data: AWS.SQS.SendMessageResult = await sqs.sendMessage(params).promise();
            console.log(data);
        } catch (ex) {
            const error: AWS.AWSError = ex as AWS.AWSError;

            console.error(error);
        }
    }

    public async onExecution(softwareName: string, token: TokenInterface, success: boolean): Promise<void> {
        if (typeof this.configuration.runQueueName === "undefined") {
            console.warn("no SQS run queue Name. Skipping monitoring.");
            return;
        }

        const sqs = new AWS.SQS();

        const params = {
            MessageBody: JSON.stringify({
                softwareName: softwareName,
                token: token.toString(),
                success: success,
            }),
            QueueUrl: `https://sqs.${this.configuration.region}.amazonaws.com/${this.configuration.accountId}/${this.configuration.runQueueName}`
        } as AWS.SQS.SendMessageRequest;

        try {
            console.log(`Sending monitoring execution signal using SQS for software ${softwareName}...`);
            const data: AWS.SQS.SendMessageResult = await sqs.sendMessage(params).promise();
            console.log(data);
        } catch (ex) {
            const error: AWS.AWSError = ex as AWS.AWSError;

            console.error(error);
        }
    }
}