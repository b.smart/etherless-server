/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {SoftwareExecutionResult, EtherlessSoftwareExecutionOperation} from "@b-smart/etherless-types";
import {SoftwareExecutionResultLoadStoreInterface} from "./SoftwareExecutionResultLoadStoreInterface";
import {EtherlessConfigurationAWSInterface} from "./EtherlessConfigurationAWSInterface";

import * as AWS from "aws-sdk";
import {SoftwareExecutionResultStoreException} from "./exceptions/SoftwareExecutionResultStoreException";
import {SoftwareExecutionResultLoadException} from "./exceptions/SoftwareExecutionResultLoadException";

export class AWSSoftwareExecutionResultLoadStore implements SoftwareExecutionResultLoadStoreInterface {

    public readonly serverConfiguration: EtherlessConfigurationAWSInterface;

    constructor(serverConfiguration: EtherlessConfigurationAWSInterface) {
        this.serverConfiguration = serverConfiguration;
    }

    withServerConfiguration(serverConfiguration: EtherlessConfigurationAWSInterface): AWSSoftwareExecutionResultLoadStore {
        return new AWSSoftwareExecutionResultLoadStore(serverConfiguration);
    }
    
    protected composeKey(executionOperation: EtherlessSoftwareExecutionOperation): string {
        return `${executionOperation.token.toString()}.json`;
    }

    async store(result: SoftwareExecutionResult, executionOperation: EtherlessSoftwareExecutionOperation): Promise<void> {
        const S3 = new AWS.S3();
        
        const s3PutParams = {
            Bucket: this.serverConfiguration.softwareExecutionResultsBucketName,
            Key: this.composeKey(executionOperation),
            Body: JSON.stringify(result)
        } as AWS.S3.PutObjectRequest;

        try {
            console.log(`Storing execution result for operation ${executionOperation.token.toString()}...`);
            const result = await S3.putObject(s3PutParams).promise();
            console.log(result);
        } catch (ex) {
            const error = ex as AWS.AWSError;
            console.error(error);

            throw new SoftwareExecutionResultStoreException(executionOperation);
        }
    }

    async load(executionOperation: EtherlessSoftwareExecutionOperation): Promise<SoftwareExecutionResult> {
        const S3 = new AWS.S3();
        
        const s3GetParams = {
            Bucket: this.serverConfiguration.softwareExecutionResultsBucketName,
            Key: this.composeKey(executionOperation)
        } as AWS.S3.GetObjectRequest;

        try {
            console.log(`Retrieving execution result for operation ${executionOperation.token.toString()}...`);
            const result: AWS.S3.GetObjectOutput = await S3.getObject(s3GetParams).promise();
            console.log(result);

            let stringifiedResult = "";

            if (result.Body instanceof Buffer) {
                stringifiedResult = result.Body.toString();
            } else if (result.Body instanceof Uint8Array) {
                stringifiedResult = result.Body.toString();
            } else if (typeof result.Body === "undefined") {
                stringifiedResult = "";
            } else if (typeof result.Body === "string") {
                stringifiedResult = result.Body;
            } else {
                console.error("Blob ?!?!?");

                throw new SoftwareExecutionResultLoadException(executionOperation);
            }
            
            const parsedResult = JSON.parse(stringifiedResult);

            return new SoftwareExecutionResult(parsedResult.statusCode, parsedResult.result);
        } catch (ex) {
            const error = ex as AWS.AWSError;
            console.error(error);

            throw new SoftwareExecutionResultLoadException(executionOperation);
        }
        
    }

}
