/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as AWS from "aws-sdk";

import {EtherlessConfigurationAWSInterface} from "./EtherlessConfigurationAWSInterface";
import {TokenInterface} from "@b-smart/etherless-types";
import {TokenManagerInterface} from "./TokenManagerInterface";
import {TokenOperationException} from "./exceptions/TokenOperationException";

export class AWSDynamoDBTokenManager implements TokenManagerInterface {

	public readonly serverConfiguration: EtherlessConfigurationAWSInterface;

	constructor(serverConfiguration: EtherlessConfigurationAWSInterface) {
	    this.serverConfiguration = serverConfiguration;
	}

	withServerConfiguration(serverConfiguration: EtherlessConfigurationAWSInterface): AWSDynamoDBTokenManager {
	    return new AWSDynamoDBTokenManager(serverConfiguration);
	}

	async storeToken(token: TokenInterface): Promise<void> {
	    const DynamoDB = new AWS.DynamoDB();
        
	    const param = {
	        TableName: this.serverConfiguration.tokenTableName,
	        Item: {
	            tkn: {
	                S: token.toString()
	            }
	        }
	    } as AWS.DynamoDB.PutItemInput;

	    try {
	        console.log("Writing token to Dynamo...");
	        const data: AWS.DynamoDB.PutItemOutput = await DynamoDB.putItem(param).promise();
	        console.log(data);
	    } catch (ex) {
	        const error: AWS.AWSError = ex as AWS.AWSError;

	        console.error(error);

	        throw new TokenOperationException(token);
	    }
	}

	async removeToken(token: TokenInterface): Promise<void> {
	    const DynamoDB = new AWS.DynamoDB();

	    const param = {
	        TableName: this.serverConfiguration.tokenTableName,
	        Key: {
	            tkn: {
	                S: token.toString()
	            }
	        }
	    } as AWS.DynamoDB.DeleteItemInput;

	    try {
	        console.log(`Deleting token ${token}...`);
	        const data: AWS.DynamoDB.DeleteItemOutput = await DynamoDB.deleteItem(param).promise();
	        console.log(data);
	    } catch (ex) {
	        const error: AWS.AWSError = ex as AWS.AWSError;

	        console.error(error);

	        throw new TokenOperationException(token);
	    }
	}

	async checkToken(token: TokenInterface): Promise<boolean> {
	    const DynamoDB = new AWS.DynamoDB();

	    const param = {
	        TableName: this.serverConfiguration.tokenTableName,
	        FilterExpression : "tkn = :tokenvalue",
	        ExpressionAttributeValues : {
	            ":tokenvalue": {
	                S: token.toString()
	            }
	        }
	    } as AWS.DynamoDB.ScanInput;

	    try {
	        console.log(`Checking for token ${token}...`);
	        const data: AWS.DynamoDB.ScanOutput = await DynamoDB.scan(param).promise();
	        console.log(data);
			
	        return (typeof data.Count !== "undefined") && (data.Count > 0);
	    } catch (ex) {
	        const error: AWS.AWSError = ex as AWS.AWSError;
            
	        console.error(error);

	        throw new TokenOperationException(token);
	    }
		
	}
}
