/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as AWS from "aws-sdk";

import {KeyLoadStoreInterface} from "./KeyLoadStoreInterface";
import {EtherlessConfigurationAWSInterface} from "./EtherlessConfigurationAWSInterface";
import {StoreKeyOperationException} from "./exceptions/StoreKeyOperationException";
import {LoadKeyOperationException} from "./exceptions/LoadKeyOperationException";
import {CheckKeyOperationException} from "./exceptions/CheckKeyOperationException";

import {DerivedKeyInterface} from "@b-smart/etherless-types";
import {DerivedKey} from "@b-smart/etherless-smart";

export class AWSKeyLoadStore implements KeyLoadStoreInterface {

    public readonly serverConfiguration: EtherlessConfigurationAWSInterface;

    constructor(serverConfiguration: EtherlessConfigurationAWSInterface) {
	    this.serverConfiguration = serverConfiguration;
    }
    
    public async store(address: string, key: DerivedKeyInterface): Promise<void> {
	    const DynamoDB = new AWS.DynamoDB();
        
	    const param = {
	        TableName: this.serverConfiguration.keysTableName,
	        Item: {
                address: {
                    S: address
                },
	            key: {
	                S: key.toString()
                }
	        }
	    } as AWS.DynamoDB.PutItemInput;

	    try {
            console.log("Writing address=>key to DynamoDB...");
	        const data: AWS.DynamoDB.PutItemOutput = await DynamoDB.putItem(param).promise();
	        console.log(data);
	    } catch (ex) {
	        const error: AWS.AWSError = ex as AWS.AWSError;

	        console.error(error);

	        throw new StoreKeyOperationException(this.serverConfiguration.keysTableName);
	    }
    }

    public async load(address: string): Promise<DerivedKeyInterface> {
	    const DynamoDB = new AWS.DynamoDB();

	    const param = {
	        TableName: this.serverConfiguration.keysTableName,
	        Key: {
	            address: {
	                S: address
	            }
	        }
	    } as AWS.DynamoDB.GetItemInput;

	    try {
            console.log("Calling AWS getItem...");
	        const data: AWS.DynamoDB.GetItemOutput = await DynamoDB.getItem(param).promise();
            console.log(data);
			
            const retrievedItem = data.Item;

            if(typeof retrievedItem === "undefined") {
                throw new LoadKeyOperationException(address.toString());
            }

            const key = retrievedItem.key.S;
            if (typeof key === "undefined") {
                throw new LoadKeyOperationException(address.toString());
            }
			
            return new DerivedKey(key);
	
	    } catch (ex) {
	        const error: AWS.AWSError = ex as AWS.AWSError;
            
	        console.error(error);

	        throw new LoadKeyOperationException(address.toString());
        }
    }
	
    public async isStored(address: string): Promise<boolean> {
        const DynamoDB = new AWS.DynamoDB();

	    const param = {
	        TableName: this.serverConfiguration.keysTableName,
            FilterExpression : "address = :addressvalue",
            ExpressionAttributeValues : {
                ":addressvalue": {
                    S: address.toString()
                }
            }
	    } as AWS.DynamoDB.ScanInput;

	    try {
            console.log(`Checking for address ${address}...`);
	        const data: AWS.DynamoDB.ScanOutput = await DynamoDB.scan(param).promise();
	        console.log(data);
			
            return (typeof data.Count !== "undefined") && (data.Count > 0);
	    } catch (ex) {
	        const error: AWS.AWSError = ex as AWS.AWSError;
            
	        console.error(error);

	        throw new CheckKeyOperationException(address.toString());
        }

    }
}// AWSKeyLoadStore