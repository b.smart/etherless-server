# etherless-server
The server for the etherless platform: targets AWS via the [serverless](https://www.serverless.com/) framework.

## Build
Build using

```sh
npm install
```

## Deploy
Make sure you have correctly configured the file config.json.

After that the command to deploy the entire stack is:

```sh
serverless deploy
```

## Testing

Local tests (mocha and chai) can be run using
```sh
npm run test
```

## References

https://www.math.unipd.it/~tullio/IS-1/2019/Progetto/C2.pdf

Software Engineering project at University of Padua, developed for [Red Babel](http://redbabel.com/), by B.smart.


