/*
 * MIT License
 * 
 * Copyright (c) 2020 B.smart
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {EthereumConfigurationServerInterface} from "./EthereumConfigurationServerInterface";
import {EtherlessConfigurationAWSInterface} from "./EtherlessConfigurationAWSInterface";
import {EthereumConnectionConfiguration} from "@b-smart/etherless-smart";
import {Context} from "aws-lambda";

const defaultConfiguration = require("./config.json");

export class EtherlessServerConfiguration extends EthereumConnectionConfiguration implements EthereumConfigurationServerInterface, EtherlessConfigurationAWSInterface {

        public readonly waitTimeout: number|undefined

        public readonly accountId: string

        readonly region: string

        public readonly softwareSourcesBucketName: string

        public readonly softwareExecutionResultsBucketName: string

        public readonly tokenBucketName: string

        public readonly tokenTableName: string

        public readonly keysTableName: string

        public readonly lambdaIAMRole: string

        public readonly lambdaRuntime: string

        public readonly lambdaMemorySize: number

        public readonly deployQueueName: string|undefined

        public readonly updateQueueName: string|undefined

        public readonly removeQueueName: string|undefined

        public readonly runQueueName: string|undefined

        public readonly privateKey: string

        constructor(
            infuraProjectID: string,
            infuraNetwork: string,
            rpcProviderAddress: string,
            smartAddress: string,
            resetToBlockNumber: number,
            waitTimeout: number|undefined,
            accountId: string,
            region: string,
            softwareSourcesBucketName: string,
            softwareExecutionResultsBucketName: string,
            tokenBucketName: string,
            tokenTableName: string,
            keysTableName: string,
            lambdaIAMRole: string,
            lambdaRuntime: string,
            lambdaMemorySize: number,
            deployQueueName: string|undefined,
            updateQueueName: string|undefined,
            removeQueueName: string|undefined,
            runQueueName: string|undefined,
            privateKey: string
        ) {
            super(
                infuraProjectID,
                infuraNetwork,
                rpcProviderAddress,
                smartAddress,
                resetToBlockNumber
            );

            this.waitTimeout = waitTimeout;
            this.accountId = accountId;
            this.region = region;
            this.softwareSourcesBucketName = softwareSourcesBucketName;
            this.softwareExecutionResultsBucketName = softwareExecutionResultsBucketName;
            this.tokenBucketName = tokenBucketName;
            this.tokenTableName = tokenTableName;
            this.keysTableName = keysTableName;
            this.lambdaIAMRole = lambdaIAMRole;
            this.lambdaRuntime = lambdaRuntime;
            this.lambdaMemorySize = lambdaMemorySize;
            this.deployQueueName = deployQueueName;
            this.updateQueueName = updateQueueName;
            this.removeQueueName = removeQueueName;
            this.runQueueName = runQueueName;
            this.privateKey = privateKey;
        }

        public static loadDefault(context: Context): EtherlessServerConfiguration {
            return new EtherlessServerConfiguration(
                defaultConfiguration.infuraProjectID,
                defaultConfiguration.network,
                defaultConfiguration.rpcProviderAddress,
                defaultConfiguration.smartAddress,
                defaultConfiguration.resetToBlockNumber,
                defaultConfiguration.timeout,
                context.invokedFunctionArn.split(":")[4],
                defaultConfiguration.region,
                defaultConfiguration.buckets.softwareSourcesBucket,
                defaultConfiguration.buckets.softwareExcecutionResultsBucket,
                defaultConfiguration.buckets.tokenBucket,
                defaultConfiguration.tables.token,
                defaultConfiguration.tables.keys,
                `${process.env.LAMBDA_IAM}`,
                defaultConfiguration.lambdaRuntime,
                defaultConfiguration.lambdaMemorySize,
                defaultConfiguration.queues.deploy,
                defaultConfiguration.queues.update,
                defaultConfiguration.queues.delete,
                defaultConfiguration.queues.run,
                defaultConfiguration.account.privateKey
            );
        }
}// EtherlessServerConfiguration

/* KEEP THE WHOLE SERVER CONFIGURATION, FROM AWS TO ETHEREUM CONNECTIONS */